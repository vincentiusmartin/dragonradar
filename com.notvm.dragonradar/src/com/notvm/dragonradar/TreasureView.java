package com.notvm.dragonradar;

import java.io.InputStream;

import android.R.string;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

public class TreasureView extends View {

	private String chestid = "0";
	private String bssid;
	private float distance;
	private float degree;
	Paint paint;
	Bitmap bitmap;
	final int IMAGE_WIDTH = 10;
	final int IMAGE_HEIGHT = 10;
	Display display;
	Context cont;
	
	@SuppressWarnings("unused")
	public TreasureView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		display = wm.getDefaultDisplay();
		distance = 100;
		degree = 45;
		
		InputStream resource = getResources().openRawResource(R.drawable.treasure);
    bitmap = BitmapFactory.decodeStream(resource);
	}
	
	public TreasureView(Context context, String chestid, String bssid, float dist, float degr) {
		super(context);
		// TODO Auto-generated constructor stub
		cont = context;
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		display = wm.getDefaultDisplay();
		this.chestid = chestid;
		this.bssid = bssid;
		distance = dist;
		degree = degr;
		
		InputStream resource = getResources().openRawResource(R.drawable.treasure);
    bitmap = BitmapFactory.decodeStream(resource);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		// super.onDraw(canvas);
		int scale = MainActivity.scale;

		if (MainActivity.iv == null)
		{
			Log.i("null", "null");
		}
		if ((display.getWidth()/2 - calculateY()*scale - IMAGE_HEIGHT*2) < display.getWidth())
		{
			canvas.drawBitmap(bitmap, 
					calculateX()*scale - IMAGE_WIDTH + display.getWidth()/2, 
					display.getWidth()/2 - calculateY()*scale - IMAGE_HEIGHT, paint);
		}
		invalidate();
	}
	
	/*@SuppressWarnings("deprecation")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			// code to change to tracking mode here
			float absis = calculateX()*MainActivity.scale - IMAGE_WIDTH + MainActivity.iv.getLeft() + MainActivity.iv.getWidth()/2;
			float ordinat = MainActivity.iv.getTop() + MainActivity.iv.getHeight()/2 - calculateY()*MainActivity.scale - IMAGE_HEIGHT;
			Log.i("Touch", "Absis = " + event.getX() + " Ordinat = " + event.getY());
			Log.i("TreasureLocation", "batas x = " + absis + "-" + (absis + IMAGE_WIDTH*2) + "batas y = " + ordinat + "-" + (ordinat + IMAGE_HEIGHT*2));
			if (event.getX() >= absis && event.getX() <= absis + IMAGE_WIDTH*2 && event.getY() >= ordinat && event.getY() <= ordinat + IMAGE_HEIGHT*2)
			{
				Intent i = new Intent(cont, TrackerActivity.class);
				i.putExtra("id", chestid);
				cont.startActivity(i);
			}
		}
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			return true;
		}
		return super.onTouchEvent(event);
	}*/
	
	public String getChestID()
	{
		return chestid;
	}
	
	public String getBssID()
	{
		return bssid;
	}
	
	public float getDistance()
	{
		return distance;
	}
	
	public float getDegree()
	{
		return degree;
	}
	
	public void setDistance(float X)
	{
		distance = X;
	}
	
	public void setDegree(float Y)
	{
		degree = Y;
	}
	
	public float calculateX()
	{
		float d = degree - MainActivity.azimuth;
		if (d < 0)
			d = 360 + d;
		
		if (d == 0 || d == 180)
		{
			return 0;
		}
		else if (d == 90)
		{
			return distance;
		}
		else if (d == 270)
		{
			return -distance;
		}
		else
		{
			return (float) (distance * Math.sin(Math.toRadians(d)));
		}
	}
	
	public float calculateY()
	{
		float d = degree - MainActivity.azimuth;
		if (d < 0)
			d = 360 + d;
		
		if (d == 90 || d == 270)
		{
			return 0;
		}
		else if (d == 0)
		{
			return distance;
		}
		else if (d == 180)
		{
			return -distance;
		}
		else
		{
			return (float) (distance * Math.cos(Math.toRadians(d)));
		}
	}
}
