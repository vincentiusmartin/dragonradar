package com.notvm.dragonradar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.view.View;

public class Compass extends View {

	  /*init variables*/
	  private Paint paint;
	  private double position = 0;
	  
	  /*image variables*/
	  /*1*/
	  private Bitmap arrow;
	  private Matrix matrix = new Matrix();
	  private double initRotate = 0;
	  /*2*/
	  private Bitmap toothpaste;
	  private Bitmap oritoothpaste;
	  
	  private float time = 0f;
	  private double distance = 0f;
	  
	  /*frame manager*/
	  int frame = 0;
	  
	  private Context context;
	  
	  /*media*/
	  private MediaPlayer beep;
	  
	  /*view*/
	  public static String longView = "0";
	  public static String latView = "0";
	  
	  public Compass(Context context) {
	    super(context);
	    arrow = BitmapFactory.decodeResource(getResources(), R.drawable.bow);
	    oritoothpaste = BitmapFactory.decodeResource(getResources(), R.drawable.toothpaste);
	    toothpaste = Bitmap.createBitmap(oritoothpaste, 0, 0, oritoothpaste.getWidth()/2, oritoothpaste.getHeight());
	    
	    setBackgroundColor(Color.BLACK);
	    time = SystemClock.uptimeMillis();
	    
	    this.context = context;
	    beep = MediaPlayer.create(context, R.raw.radar);
	    
	    init();
	  }

	  private void init() {
	    paint = new Paint();
	    paint.setAntiAlias(true);
	    paint.setStrokeWidth(2);
	    paint.setTextSize(25);
	    paint.setStyle(Paint.Style.STROKE);
	    paint.setColor(Color.WHITE);
	    
	    beep.start();
	    beep.setLooping(true);
	    beep.setVolume(0, 0);
	  }
	  
	  public MediaPlayer getBeep(){
		  return beep;
	  }

	  @Override
	  protected void onDraw(Canvas canvas) {
		canvas.drawText(longView, 20, 50, paint);
		canvas.drawText(latView, 20, 80, paint);
		canvas.drawText("distance :" + distance, 20, 110, paint);
		  
	    int xPoint = getMeasuredWidth() / 2;
	    int yPoint = getMeasuredHeight() / 2;
	    
	    /*create circle*/
	    float radius = (float) (Math.max(xPoint, yPoint) * 0.6);
	    canvas.drawCircle(xPoint, yPoint, radius, paint);
	    canvas.drawRect(0, 0, getMeasuredWidth(), getMeasuredHeight(), paint);

	    /*time manager*/
	    beep.setVolume((100-(float)distance)/100,(100-(float)distance)/100);
	    if(SystemClock.uptimeMillis() - time > ((distance/100) * 2000)){
	    	time = SystemClock.uptimeMillis();
	    	frame++;
	    	if(frame > 1){
	    		frame = 0;
	    	}
	    	toothpaste = Bitmap.createBitmap(oritoothpaste, frame * 100, 0, oritoothpaste.getWidth()/2, oritoothpaste.getHeight());
	    }

	    /*adjust image*/
	    if(position != initRotate){
	    	initRotate = position;
	    	matrix.setRotate((float)initRotate, arrow.getWidth()/2, arrow.getHeight()/2);
	    	matrix.postTranslate(-arrow.getWidth()/2, -arrow.getHeight()/2);
	    	matrix.postTranslate(xPoint, yPoint);
	    }
	    canvas.drawBitmap(arrow, matrix, null);
	    canvas.drawBitmap(toothpaste, getWidth()-110, getHeight()-140, paint);

	    /*set line position*/
	    // 3.143 is a good approximation for the circle
	    canvas.drawLine(xPoint, yPoint,
	        (float) (xPoint + radius
	            * Math.sin((double) (-position) / 180 * 3.143)),
	        (float) (yPoint - radius
	            * Math.cos((double) (-position) / 180 * 3.143)), paint);

	    canvas.drawText(String.valueOf(position), xPoint, yPoint, paint);
	  }

	  public void updateData(double position,double distance) {
	    this.position = position;
	    this.distance = distance;
	    invalidate();
	  }
}
