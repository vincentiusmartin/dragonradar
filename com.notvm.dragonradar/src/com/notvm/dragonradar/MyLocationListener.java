package com.notvm.dragonradar;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class MyLocationListener implements LocationListener {
    public static String text = "0";
    private double Lat;
    private double Long;
    
    public MyLocationListener(){

    }
    
	@Override
	public void onLocationChanged(Location location) {
		Lat = location.getLatitude();
		Long = location.getLongitude();
		Compass.longView = "Longitude = " + Lat;
		Compass.latView = "Latitude = " + Long;
	}

	@Override
	public void onProviderDisabled(String provider) {
		//Toast.makeText(m.getApplicationContext(),
	               //"Gps Disabled", Toast.LENGTH_SHORT).show();
		 Log.d("Logs : ", "GPS DISABLED");

	}

	@Override
	public void onProviderEnabled(String provider) {
		//Toast.makeText(m.getApplicationContext(),
	               //"Gps ClassName.thisDisabled", Toast.LENGTH_SHORT).show();
		Log.d("Logs : ", "GPS ENABLED");

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
	
	//getter
	public double getLat(){
		return this.Lat;
	}
	
	public double getLong(){
		return this.Long;
	}

}
