package com.notvm.dragonradar;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class TrackerActivity extends Activity{

	  private static SensorManager sensorService;
	  private Compass compassView;
	  private Sensor sensor;
	  private TreasureView treasure = null;
	  
	  private Location objLocation;
	  private Location myLoc;
	  
	  //public static JSONArray treasuresjson = new JSONArray();
	  private MyLocationListener locListener;
	  private WifiManager wifiManager;
	  //private HttpThread ht;
	  
	/** Called when the activity is first created. */

	  public void update(){
	  	//Toast.makeText(this, "syalala : " + myLoc.distanceTo(objLocation), Toast.LENGTH_SHORT).show();
	  }
	  
	  @SuppressWarnings("deprecation")
	  @Override
	  public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
	    Bundle extras = getIntent().getExtras();
	    if (extras == null) {
	      return;
	    }
		  String chestid = extras.getString("id");
	    
	    /*use the location manager class to obtain GPS location*/
		LocationManager locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		locListener = new MyLocationListener();
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
//	    Criteria criteria = new Criteria();
//	    String provider = locManager.getBestProvider(criteria, false);
		Location location = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
	    compassView = new Compass(this);
	    Compass.longView = "Longitude = " + location.getLongitude();
	    Compass.latView = "Latitude = " + location.getLatitude();
	    setContentView(compassView);
	    
	    //ht = new HttpThread(this, "http://milestone.if.itb.ac.id/pbd/index.php", "8570f871f32b197d63d344730a808202", "retrieve", locListener.getLat(), locListener.getLong());
	    
	    sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	    sensor = sensorService.getDefaultSensor(Sensor.TYPE_ORIENTATION);
	    if (sensor != null) {
	      sensorService.registerListener(mySensorEventListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
	      Log.i("Compass MainActivity", "Registerered for ORIENTATION Sensor");
	    } else {
	      Log.e("Compass MainActivity", "Registerered for ORIENTATION Sensor");
	      Toast.makeText(this, "ORIENTATION Sensor not found",Toast.LENGTH_LONG).show();
	      finish();
	    }
	    
	    new Thread(new HttpThread(this, "http://milestone.if.itb.ac.id/pbd/index.php", "8570f871f32b197d63d344730a808202", "retrieve", (float)location.getLatitude(), (float)location.getLongitude())).start();
	    
	    if (MainActivity.treasuresjson != null)
	    {
	    	Boolean flag = false;
		    for (int i = 0; i < MainActivity.treasuresjson.length(); i++)
		    {
		    	try {
		    		if (!flag && ((JSONObject) MainActivity.treasuresjson.get(i)).getString("id").equals(chestid))
		    		{
		    			treasure = new TreasureView(this, 
								((JSONObject) MainActivity.treasuresjson.get(i)).getString("id"), 
//		    					"4b81f18f6a422d56b9e838f87b1cea4d", 
								((JSONObject) MainActivity.treasuresjson.get(i)).getString("bssid"), 
								(float) ((JSONObject) MainActivity.treasuresjson.get(i)).getDouble("distance"), 
								(float)((JSONObject) MainActivity.treasuresjson.get(i)).getDouble("degree"));
		    			flag = true;
		    		}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    }
	    }
		
		/*init my position*/
	    myLoc = new Location(LocationManager.GPS_PROVIDER);
	    myLoc.setLongitude(location.getLongitude());
		myLoc.setLatitude(location.getLatitude());
	    
		/*obj position*/
		objLocation = new Location(LocationManager.GPS_PROVIDER);
	    objLocation.setLatitude(myLoc.getLatitude() + (Math.cos(treasure.getDegree()) * (treasure.getDistance()/111132.9)));
	    objLocation.setLongitude(myLoc.getLongitude() + (Math.sin(treasure.getDegree()) * (treasure.getDistance()/111412.84)));
	    //objLocation.setLatitude(Math.asin( Math.sin(Math.toRadians(locListener.getLat()))*Math.cos(treasure.getDistance()/EARTH_RADIUS) + Math.cos(Math.toRadians(locListener.getLat()))*Math.sin((treasure.getDistance()/1000)/EARTH_RADIUS)*Math.cos(Math.toRadians(treasure.getDegree()))));
	    //objLocation.setLongitude(Math.toRadians(locListener.getLong()) + Math.atan2(Math.sin(Math.toRadians(treasure.getDegree()))*Math.sin(treasure.getDistance()/EARTH_RADIUS)*Math.cos(Math.toRadians(locListener.getLat())), Math.cos((treasure.getDistance()/1000)/EARTH_RADIUS)-Math.sin(Math.toRadians(locListener.getLat()))*Math.sin(Math.toRadians(objLocation.getLatitude()))));	

	  }
	  
//	  public void getWifiInfo(){
//	  	
//			BroadcastReceiver receiver = new BroadcastReceiver() {
//				@Override
//				public void onReceive(Context context, Intent intent) {
//					List<ScanResult> results = wifiManager.getScanResults();
//					
//					
//					
//					ScanResult wifi2 = null;
//					for (ScanResult result : results) {
//						if (bssid.equals(result.BSSID)) {
//							wifi = result.level;
//							wifi2 = result;
//						}
//					}
//					
//					if (wifi2 != null) {
//						//Toast.makeText(context, String.format("FOUND: %s\n%s\n%d", bssid, bssid, wifi), Toast.LENGTH_LONG).show();
//					} else {
//						//Toast.makeText(context, "NOT FOUND", Toast.LENGTH_LONG).show();
//						wifi = 0;
//					}
//				}
//			};
//			registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
//			wifiManager.startScan();
//		}
	  
	  @Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_tracker, menu);
			return true;
		}

	  private SensorEventListener mySensorEventListener = new SensorEventListener() {

	    @Override
	    public void onAccuracyChanged(Sensor sensor, int accuracy) {
	    }

	    @Override
	    public void onSensorChanged(SensorEvent event) {
	      // angle between the magnetic north directio
	      // 0=North, 90=East, 180=South, 270=West
	    	
		  float azimuth = event.values[0]; //degree to north
		  
		  myLoc.setLongitude(locListener.getLong());
		  myLoc.setLatitude(locListener.getLat());
		  //Log.i("kyakyaaa ", "total : " + brng);
		  /*calculate*/
		  //float treasuredegree = azimuth - treasure.getDegree();
		  
		  float degree = myLoc.bearingTo(objLocation) - azimuth;
		  //System.out.println("myloc : " + myLoc.getLongitude() + " and " + myLoc.getLatitude());
		  
		  if(degree < 0){
			  degree += 360;
		  }
		  
	      update();
	      if (treasure != null)
	      {
		      compassView.updateData(degree, myLoc.distanceTo(objLocation));
	      }
	    }
	  };
	  
	  @Override
	  public boolean onKeyDown(int keyCode, KeyEvent event)
	  {
	      if ((keyCode == KeyEvent.KEYCODE_BACK))
	      {
	      		compassView.getBeep().stop(); //stop sound
	          finish();
	      }
	      return super.onKeyDown(keyCode, event);
	  }

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
		    // Handle item selection
		    switch (item.getItemId()) {
		        case R.id.menu_camera:
		      		Intent intent = new Intent(this, CameraActivity.class);
		      		intent.putExtra("chestid", treasure.getChestID());
		      		intent.putExtra("bssid", treasure.getBssID());
		      		startActivity(intent);
		      		
	            return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
	  
	  @SuppressWarnings("deprecation")
		@Override
	  protected Dialog onCreateDialog(int id) {
			switch (id) {
		    case 1:
		    	Intent i = new Intent(this, CameraActivity.class);
					i.putExtra("id", treasure.getChestID());
					startActivity(i);
		      break;
	    }
	    return super.onCreateDialog(id);
	  }
	  
	  @Override
	  protected void onDestroy() {
	    super.onDestroy();
	    if (sensor != null) {
	      sensorService.unregisterListener(mySensorEventListener);
	      compassView.getBeep().stop();
	      compassView.getBeep().release();
	    }
	  }
}
