package com.notvm.dragonradar;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Display;
//import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements LocationListener {

	private RelativeLayout layout;
	//private int SCREEN_WIDTH;
	//private int SCREEN_HEIGHT;
	private TextView latituteField;
  private TextView longitudeField;
  private TextView angleField;
  
	private LocationManager locationManager;
	private String provider;
	private float currentlatitude;
	private float currentlongitude;
	
	private SensorManager sensorManager;
	private Sensor sensor;
	
	public static float azimuth = 0;
	public static int scale = 1;
	public static JSONArray treasuresjson = null;
	public static int chestcount = 0;

	final int IMAGE_WIDTH = 10;
	final int IMAGE_HEIGHT = 10;
	
	Display display;
	public static ImageView iv;
	private int SCREEN_WIDTH;
	private int SCREEN_HEIGHT;
	
	private ArrayList<TreasureView> treasures = new ArrayList<TreasureView>();
//	TreasureView treasure1;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		display = getWindowManager().getDefaultDisplay();
		SCREEN_WIDTH = display.getWidth();
		SCREEN_HEIGHT = display.getHeight();
		
		iv = (ImageView) findViewById(R.id.imageView1);
		
		Bundle extras = getIntent().getExtras();
    if (extras != null) {
  	  String response = extras.getString("result");
      Toast.makeText(this, response, Toast.LENGTH_LONG).show();
    }
		
		latituteField = (TextView) findViewById(R.id.latitudenumber);
    longitudeField = (TextView) findViewById(R.id.longitudenumber);

    initLocation();
    initSensor();
    initTreasures();
    
//		treasure1 = new TreasureView(this, "id1", "bss1", 80, (float)(45.0));
//		layout.addView(treasure1);
	}

	public void initLocation()
	{
		// Get the location manager
    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    
    // Define the criteria how to select the location provider -> use default
//    Criteria criteria = new Criteria();
//    provider = locationManager.getBestProvider(criteria, false);
    provider = locationManager.getProvider(LocationManager.GPS_PROVIDER).getName();
    Location location = locationManager.getLastKnownLocation(provider);
    
    if (location != null) 
    {
    	Log.i("GPS", "Latitude = " + location.getLatitude() + ", Longitude = " + location.getLongitude());
    	onLocationChanged(location);
    }
    else
    {
    	currentlatitude = 0;
    	currentlongitude = 0;
    	latituteField.setText("Location not available");
      longitudeField.setText("Location not available");
    }
	}
	
	@SuppressWarnings("deprecation")
	public void initSensor()
	{
		angleField = (TextView) findViewById(R.id.anglenumber);
    sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    
    if (sensor != null) {
    	sensorManager.registerListener(mySensorEventListener, sensor,
          SensorManager.SENSOR_DELAY_NORMAL);
    } else {
      Toast.makeText(this, "ORIENTATION Sensor not found",
          Toast.LENGTH_LONG).show();
      finish();
    }
	}
	
	public void initTreasures()
	{
		new Thread(new HttpThread(this, "http://milestone.if.itb.ac.id/pbd/index.php", "8570f871f32b197d63d344730a808202", "retrieve", currentlatitude, currentlongitude)).start();
    
		layout = (RelativeLayout) findViewById(R.id.layout1);
		
		treasures.clear();
    if (treasuresjson != null)
    {
	    for (int i = 0; i < treasuresjson.length(); i++)
	    {
	    	try {
					TreasureView temp = new TreasureView(this, 
							((JSONObject) treasuresjson.get(i)).getString("id"), 
							((JSONObject) treasuresjson.get(i)).getString("bssid"), 
							(float) ((JSONObject) treasuresjson.get(i)).getDouble("distance"), 
							(float)((JSONObject) treasuresjson.get(i)).getDouble("degree"));
					
					Log.i("Chest ID", temp.getChestID());
					treasures.add(temp);
					layout.addView(temp);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    }
    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu_scale:
            showDialog(1);
            return true;
	        case R.id.menu_reset:
	        	showDialog(2);
	        	return true;
	        case R.id.menu_count:
	        	showDialog(3);
	        	return true;	
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	@SuppressWarnings("deprecation")
	@Override
  protected Dialog onCreateDialog(int id) {
		switch (id) {
	    case 1:
	      // Create out AlertDialog
	    	CharSequence[] items = {"1","5","10"};
	    	
	      Builder builder = new AlertDialog.Builder(this);
	      builder.setTitle("Select the Scale");
	      builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
	      	public void onClick(DialogInterface dialog, int item) {
	      		switch(item)
	          {
	              case 0:
	                scale = 1;
	                 break;
	              case 1:
	              	scale = 5;
	                break;
	              case 2:
	              	scale = 10;
	                break;              
	          }
	          dialog.dismiss();    
	      	}
	      });
	      AlertDialog dialog = builder.create();
	      dialog.show();
	      break;
	    case 2:
	    	Builder builder2 = new AlertDialog.Builder(this);
	    	builder2.setTitle("Are you sure you want to reset the count?");
	    	builder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	    		public void onClick(DialogInterface dialog, int item)
	    		{
	    			resetCount();
	    			dialog.dismiss();
	    		}
	    	});
	    	builder2.setNegativeButton("No", null);
	    	AlertDialog dialog2 = builder2.create();
	    	dialog2.show();
	    	break;
	    case 3:
	    	Toast.makeText(this, "Number of chest = " + getChestCount(), Toast.LENGTH_LONG).show();
	    	break;
    }
    return super.onCreateDialog(id);
  }
	
//	@SuppressWarnings("deprecation")
//	public void savesBattery()
//	{
//		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//		PowerManager.WakeLock wakeLock = pm.newWakeLock(
//		        PowerManager.SCREEN_DIM_WAKE_LOCK, "My wakelook");
//		// This will make the screen and power stay on
//		// This will release the wakelook after 1000 ms
//		wakeLock.acquire();
//		 
//	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub

		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			return true;
		}
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			// code to change to tracking mode here
			Log.i("XY", event.getX() + " " + event.getY());
			
			if (event.getY() >= SCREEN_HEIGHT - findViewById(R.id.refresh).getHeight())
			{
				refreshtreasures(findViewById(R.id.refresh));
			}

			if (iv == null)
			{
				Log.i("null2", "null2");
			}
			
			for (int i = 0; i < treasures.size(); i++)
			{
				float absis = treasures.get(i).calculateX()*scale - IMAGE_WIDTH + (display.getWidth()/2);
				float ordinat = (display.getWidth()/2) - treasures.get(i).calculateY()*scale - IMAGE_HEIGHT;
				String s= "absis = " + absis + " x = " + event.getX() + "\n ordinat = " + ordinat + " y = " + event.getY();
//				Toast.makeText(this, s, Toast.LENGTH_LONG).show();
				if (event.getX() >= absis-50 && event.getX() <= absis+50 + IMAGE_WIDTH*2 && event.getY()-100 >= ordinat-50 && event.getY() <= ordinat+50 + IMAGE_HEIGHT*2)
				{
					Log.i("Keteken", "Keteken");
					Intent intent = new Intent(this, TrackerActivity.class);
					intent.putExtra("id", treasures.get(i).getChestID());
					startActivity(intent);
					break;
				}
			}
//			Log.i("Touch", "Absis = " + event.getX() + " Ordinat = " + event.getY());
//			Log.i("TreasureLocation", "batas x = " + absis + "-" + (absis + IMAGE_WIDTH*2) + "batas y = " + ordinat + "-" + (ordinat + IMAGE_HEIGHT*2));
		}
		return super.onTouchEvent(event);
	}
	
	/* Request updates at startup */
  @Override
  protected void onResume() {
    super.onResume();
    sensorManager.unregisterListener(mySensorEventListener);
    locationManager.requestLocationUpdates(provider, 400, 1, this);
  }
  
  /* Remove the locationlistener updates when Activity is paused */
  @Override
  protected void onPause() {
    super.onPause();
    sensorManager.registerListener(mySensorEventListener, sensor,
        SensorManager.SENSOR_DELAY_NORMAL);
    locationManager.removeUpdates(this);
  }

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		currentlatitude = (float) location.getLatitude();
    currentlongitude = (float) (location.getLongitude());
    latituteField.setText(String.valueOf(currentlatitude));
    longitudeField.setText(String.valueOf(currentlongitude));
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Disabled provider " + provider,
        Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Enabled new provider " + provider,
        Toast.LENGTH_SHORT).show();		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

	public void refreshtreasures(View view)
	{
		if (view.getId() == R.id.refresh)
			initTreasures();
	}
	
	public int getScale()
	{
		return scale;
	}
	
	public int getChestCount()
	{
		new Thread(new HttpThread(this, "http://milestone.if.itb.ac.id/pbd/index.php", "8570f871f32b197d63d344730a808202", "number")).start();
		return chestcount;
	}
	
	public void resetCount()
	{
		new Thread(new HttpThread(this, "http://milestone.if.itb.ac.id/pbd/index.php", "8570f871f32b197d63d344730a808202", "reset")).start();
		Toast.makeText(this, "Chest count has been reseted.", Toast.LENGTH_LONG).show();
	}
	
	private SensorEventListener mySensorEventListener = new SensorEventListener() {

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
      // angle between the magnetic north direction
      // 0=North, 90=East, 180=South, 270=West
      azimuth = event.values[0];
      angleField.setText(String.valueOf(azimuth));
    }
  };
}
