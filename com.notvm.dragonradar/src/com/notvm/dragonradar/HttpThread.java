package com.notvm.dragonradar;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

public class HttpThread implements Runnable {
	
	Context context;
	private String url;
	private String groupid;
	private String action;
	private float latitude;
	private float longitude;
	private String chestid;
	private String bssid;
	private String wifi;
	private File imgfile;
	
	public HttpThread(Context cont, String url, String groupid, String action) {
		// TODO Auto-generated constructor stub
		context = cont;
		this.url = url;
		this.groupid = groupid;
		this.action = action;
	}
	
	public HttpThread(Context cont, String url, String groupid, String action, float lat, float lng) {
		// TODO Auto-generated constructor stub
		context = cont;
		this.url = url;
		this.groupid = groupid;
		this.action = action;
		this.latitude = lat;
		this.longitude = lng;
	}
	
	public HttpThread(Context cont, String url, String groupid, String action, String chest_id, File image, String bssid, int wifi ) {
		// TODO Auto-generated constructor stub

		context = cont;
		this.url = url;
		this.groupid = groupid;
		this.action = action;
		this.chestid = chest_id;
		this.bssid = bssid;
		this.wifi = "" + wifi;
		this.imgfile = image;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
//		String s = "new";
//		JSONArray json = new JSONArray(s);
		Looper.prepare();
		
		try {
			if (action.equals("reset"))
			{
				url += "?group_id=" + groupid + "&action=" + action;
			}
			else if (action.equals("retrieve"))
			{
				url += "?group_id=" + groupid + "&action=" + action + "&latitude=" + latitude + "&longitude=" + longitude;
			}
			else if (action.equals("number"))
			{
				url += "?group_id=" + groupid + "&action=" + action;
			}
			else if (action.equals("acquire"))
			{
				HttpParams params = new BasicHttpParams();
		    params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				HttpClient httpClient = new DefaultHttpClient(params);
				
				HttpPost httpPost = new HttpPost(url);
				MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				entity.addPart("group_id", new StringBody(groupid));
				entity.addPart("chest_id",new StringBody(chestid));
				entity.addPart("file",new FileBody(imgfile));
				entity.addPart("bssid",new StringBody(bssid));
				entity.addPart("wifi",new StringBody(wifi));
				entity.addPart("action", new StringBody("acquire"));
				httpPost.setEntity(entity);
				
				httpClient.execute(httpPost, new ResponseHandler<Object>() {
					@Override
					public Object handleResponse(HttpResponse response)
							throws ClientProtocolException, IOException {
						// TODO Auto-generated method stub
						HttpEntity respEntity = response.getEntity();
						String responseString = EntityUtils.toString(respEntity);
						try {
							JSONObject json = new JSONObject(responseString);
							Log.i("halo : ", ""+json.getString("status").toString().equals("success"));
							if (json.getString("status").toString().equals("success"))
							{
								Toast.makeText(context, "Acquiring treasure successful", Toast.LENGTH_SHORT).show();
								Log.i("Request success", "Acquiring treasure successful");
								CameraActivity.response = "Acquiring treasure successful";
							}
							else
							{
								Toast.makeText(context, "Acquiring treasure failed with error message: ", Toast.LENGTH_SHORT).show();
								Log.i("Request failed", "Acquiring treasure failed");
								CameraActivity.response = "Acquiring treasure failed with error message :"+json.getString("description");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return null;
						}
					}
				);
			}
			
			if (!action.equals("acquire"))
			{
				URL newurl = new URL(url);
				HttpURLConnection con = (HttpURLConnection) newurl.openConnection();
				Toast.makeText(context, url, Toast.LENGTH_LONG).show();
				if (action.equals("reset"))
				{
					con.setRequestMethod("POST");
				}
				else if (action.equals("retrieve") || action.equals("number"))
				{
					con.setRequestMethod("GET");
				}
				con.connect();
				JSONObject json = readStream(con.getInputStream());
			
				if (action.equals("reset"))
				{
					if (json.getString("status").equals("success"))
					{
						Toast.makeText(context, "Reset success", Toast.LENGTH_SHORT).show();
					}
					else
					{
						Toast.makeText(context, "Reset failed", Toast.LENGTH_SHORT).show();
					}
				}
				else if (action.equals("retrieve"))
				{
					if (json.getString("status").equals("success"))
					{
						JSONArray treasures = null;
						if (!json.get("data").equals(null))
						{
							treasures = json.getJSONArray("data");
						}
						
						if (treasures != null)
						{
							MainActivity.treasuresjson = treasures;
							Toast.makeText(context, "Retrieve success", Toast.LENGTH_SHORT).show();
						}
						else
						{
							Toast.makeText(context, "There is no treasure nearby", Toast.LENGTH_LONG).show();
						}
					}
					else
					{
						Toast.makeText(context, "Retrieve failed", Toast.LENGTH_SHORT).show();
					}
				}
				else if (action.equals("number"))
				{
					if (json.getString("status").equals("success"))
					{
						int count = json.getInt("data");
						MainActivity.chestcount = count;
						Toast.makeText(context, "Getting unachieved chest count success", Toast.LENGTH_SHORT).show();
					}
					else
					{
						Toast.makeText(context, "Getting unachieved chest count success", Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
		catch (Exception e) 
		{
		  e.printStackTrace();
		}
	}
	
	public JSONObject readStream(InputStream in)
	{
		BufferedReader reader = null;
		JSONObject json = new JSONObject();
	  try {
	    reader = new BufferedReader(new InputStreamReader(in));
	    String line = "";
	    if ((line = reader.readLine()) != null) {
	      json = new JSONObject(line);
	    }
	  } catch (IOException e) {
	    e.printStackTrace();
	  } catch (JSONException e) {
			e.printStackTrace();
		} finally {
	    if (reader != null) {
	      try {
	        reader.close();
	      } catch (IOException e) {
	        e.printStackTrace();
	        }
	    }
	  }
	  return json;
	}

}
