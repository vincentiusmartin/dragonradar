package com.notvm.dragonradar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public class CameraActivity extends Activity {
	
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private Location location;
	
	private String chestid;
	private String bssid;
	File file;
	int wifi = 0;
	public static String response = "";

	WifiManager wifiManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		if (isIntentAvailable(this, MediaStore.ACTION_IMAGE_CAPTURE )){
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
			boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if (!enabled) {
				Toast.makeText(this, "provider not enabled!", Toast.LENGTH_LONG).show();
			} else{
				Criteria criteria = new Criteria();
				String provider = service.getBestProvider(criteria, false);
				location = service.getLastKnownLocation(provider);
				startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
			}
		}
		wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
		Bundle extras = getIntent().getExtras();
    if (extras == null) {
      return;
    }
		chestid = extras.getString("chestid");
		bssid = extras.getString("bssid");
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
	    	Bitmap bitmap = (Bitmap) data.getExtras().get("data");
	    	Bitmap bitmap_resize = Bitmap.createScaledBitmap(bitmap, 480, 640, true);
	    	try {
					String root = Environment.getExternalStorageDirectory().toString();
	    			File myDir = new File(root + "/saved_images");
	    			myDir.mkdirs();
					String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
					String imageFileName = timeStamp + ".jpeg";
					file = new File(myDir, imageFileName);
	    	       
					FileOutputStream out = new FileOutputStream(file);
					bitmap_resize.compress(Bitmap.CompressFormat.JPEG, 100, out);
					out.flush();
					out.close();
					locationToExif(file.getAbsolutePath(),location);
	    	       
					getWifiInfo();
					response = new String("");
					new Thread(new HttpThread(this, "http://milestone.if.itb.ac.id/pbd/index.php", "8570f871f32b197d63d344730a808202", "acquire", chestid, file, bssid, wifi)).start();
					Intent i = new Intent(this,MainActivity.class);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					while (response.isEmpty()){
						Thread.sleep(3000);
					}
					Log.i("msg", "sudah keluar dari while");
					i.putExtra("result", response);
					startActivity(i);
					finish();
	    	       
	    	} catch (Exception e) {
	    	       e.printStackTrace();
	    	}
	    }
	}
	
public void getWifiInfo(){
		BroadcastReceiver receiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				List<ScanResult> results = wifiManager.getScanResults();
				
				ScanResult wifi_result = null;
				for (ScanResult result : results) {
					if (bssid.equals(result.BSSID)) {
						wifi = result.level;
						wifi_result = result;
					}
				}
				
				if (wifi_result != null) {
					//Toast.makeText(context, String.format("FOUND: %s\n%s\n%d", bssid, bssid, wifi), Toast.LENGTH_LONG).show();
				} else {
					//Toast.makeText(context, "NOT FOUND", Toast.LENGTH_LONG).show();
					wifi = 0;
				}
			}
		};
		registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		wifiManager.startScan();
	}
	
	/* check whether an app can handle intent */
	public static boolean isIntentAvailable(Context context, String action){
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list =
				packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}
	
	public String decimalToDMS(double coord) {  
		  coord = coord > 0 ? coord : -coord;  // -105.9876543 -> 105.9876543
		  String sOut = Integer.toString((int)coord) + "/1,";   // 105/1,
		  coord = (coord % 1) * 60;         // .987654321 * 60 = 59.259258
		  sOut = sOut + Integer.toString((int)coord) + "/1,";   // 105/1,59/1,
		  coord = (coord % 1) * 60000;             // .259258 * 60000 = 15555
		  sOut = sOut + Integer.toString((int)coord) + "/1000";   // 105/1,59/1,15555/1000
		  return sOut;
	}
	
	public void locationToExif(String flNm, Location loc) {
		  try {
		    ExifInterface ef = new ExifInterface(flNm);
		    ef.setAttribute(ExifInterface.TAG_GPS_LATITUDE, decimalToDMS(loc.getLatitude()));
		    ef.setAttribute(ExifInterface.TAG_GPS_LONGITUDE,decimalToDMS(loc.getLongitude()));
		    if (loc.getLatitude() > 0) 
		      ef.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N"); 
		    else              
		      ef.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
		    if (loc.getLongitude()>0) 
		      ef.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");    
		     else             
		       ef.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
		    ef.saveAttributes();
		  } catch (IOException e) {}         
		}

}
